# Nely Dwi Agustin



### Hi there 👋
I am a Bachelor of Information Engineering graduate with solid experience in software development and project management.

Over the course of my career, I have a track record of Software Developer projects, from a project e-commerce to utilitas.

Beyond my pursuits, I am deeply committed to broadening my skill set. Currently undertaking a part-time bootcamp in Data Analyst at Digital Talent Scholarship, I am dedicated to gaining a deeper understanding of the Data Analyst that underpin effective applications planning.

<br>If you are interested, follow my [Linkedin](https://www.linkedin.com/in/nelydwiagustin/) account.</br>
<br>GitHub & GitLab : nee18</br>

### Tech Stack
<p align="center">
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/javascript/javascript.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/nodejs/nodejs.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/npm/npm.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/express/express.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/postgresql/postgresql.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/git/git.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/linux/linux.png"></code>
<code><img height="20" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/webpack/webpack.png"></code>
</p>
